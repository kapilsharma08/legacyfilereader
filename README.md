# LegacyFileReader

This repository is maintaining the code for legacy file reader and displays the lines which have post code in the files as an output.

Main class : LegacyFileReaderApplication.java

This class contains main method which reades the files from a particular location. 

There is hard coded location C:\\LegaceFileLocation for files to be put.

This code supports only txt files. Support for XML, CSV is provided. 