package com.binck.legacyFileReader;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.binck.legacyFileReader.constant.FileConstant;
import com.binck.legacyFileReader.factory.FileReaderFactory;
import com.binck.legacyFileReader.processer.LegacyFileContentProcessor;
import com.binck.legacyFileReader.reader.LegacyFileReader;

public class LegacyFileReaderApplication {

	Logger logger = Logger.getLogger("LegacyFileReaderApplication");

	public static void main(String[] args) {

		LegacyFileReaderApplication legacyFileReaderApplication = new LegacyFileReaderApplication();
		legacyFileReaderApplication.processLegacyFilesForPostCode();
	}

	private void processLegacyFilesForPostCode() {
		try {
			List<Path> filePaths = Files.list(Paths.get(FileConstant.FILE_DIRECTORY)).filter(Files::isRegularFile)
					.collect(Collectors.toList());

			if (filePaths.size() == 0) {
				logger.log(Level.INFO, "Nothing to process in given directory");
			} else {
				for (Path filePath : filePaths) {
					logger.log(Level.INFO, "Processing file : " + filePath.getFileName());
					
					LegacyFileReader reader = getReader(filePath.getFileName());
					
					if (reader == null) {
						logger.log(Level.WARNING, "This application does not support parsing of following file : "
								+ filePath.getFileName());
						return;
					}
					Stream<String> dataStream = reader.fileTypeReader(filePath);

					if (dataStream != null) {
						LegacyFileContentProcessor legacyFileContentProcessor = new LegacyFileContentProcessor();
						
						List<String> linesWithPostCode = legacyFileContentProcessor.processContent(dataStream);
						
						linesWithPostCode.forEach(System.out::println);
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private LegacyFileReader getReader(Path fileName) {
		return FileReaderFactory.getFileReader(fileName.toString());
	}
}
