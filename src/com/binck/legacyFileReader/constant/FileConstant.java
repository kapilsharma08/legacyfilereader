package com.binck.legacyFileReader.constant;

public class FileConstant {

	
	public static final String FILE_DIRECTORY = "C:\\LegaceFileLocation";

	public static final String CSV_FILE_EXTENSION = "csv";
	public static final String XML_FILE_EXTENSION = "txt";
	public static final String TXT_FILE_EXTENSION = "xml";
	
	public static final String POST_CODE_PATTERN = "(.*?)(NL-)?(\\d{4})\\s*([A-Z]{2})?\\s+(.*?)";
}
