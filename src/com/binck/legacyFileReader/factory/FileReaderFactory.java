package com.binck.legacyFileReader.factory;

import com.binck.legacyFileReader.constant.FileConstant;
import com.binck.legacyFileReader.reader.LegacyFileReader;
import com.binck.legacyFileReader.reader.impl.CSVReader;
import com.binck.legacyFileReader.reader.impl.TextReader;
import com.binck.legacyFileReader.reader.impl.XMLReader;

public class FileReaderFactory {

	/**
	 * @param filepath
	 * @return based on the extension of file it returns file reader, if do not support any extension than returns null
	 */
	public static LegacyFileReader getFileReader(String filepath) {

		if (filepath.endsWith(FileConstant.CSV_FILE_EXTENSION)) {
			return new CSVReader();
		} else if (filepath.endsWith(FileConstant.TXT_FILE_EXTENSION))
			return new TextReader();
		else if (filepath.endsWith(FileConstant.XML_FILE_EXTENSION))
			return new XMLReader();
		return null;
	}
}
