package com.binck.legacyFileReader.processer;

import java.util.List;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.binck.legacyFileReader.constant.FileConstant;

public class LegacyFileContentProcessor {

	
	/**
	 * @param linesOfFile
	 * @return all the lines which contains post code
	 */
	public List<String> processContent(Stream<String> linesOfFile) {

		Predicate<String> postCodeFilter = Pattern.compile(FileConstant.POST_CODE_PATTERN).asPredicate();

		return linesOfFile.filter(postCodeFilter).collect(Collectors.<String>toList());
	}
}
