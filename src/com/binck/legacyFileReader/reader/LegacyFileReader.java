package com.binck.legacyFileReader.reader;

import java.nio.file.Path;
import java.util.stream.Stream;

public interface LegacyFileReader {
	
	public Stream<String> fileTypeReader(Path filePath);
}
