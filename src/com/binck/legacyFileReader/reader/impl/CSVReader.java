package com.binck.legacyFileReader.reader.impl;

import java.nio.file.Path;
import java.util.stream.Stream;

import com.binck.legacyFileReader.reader.LegacyFileReader;

public class CSVReader implements LegacyFileReader{
	
	/**
	 * @param path of the csv file
	 * @return stream of data exist in the file
	 * for now we are not implementing this one
	 */
	@Override
	public Stream<String> fileTypeReader(Path filePath) {
		return null;
	}
}
