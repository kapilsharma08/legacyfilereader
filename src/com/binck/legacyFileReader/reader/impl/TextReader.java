package com.binck.legacyFileReader.reader.impl;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

import com.binck.legacyFileReader.reader.LegacyFileReader;

public class TextReader implements LegacyFileReader {

	/**
	 * @param path of the text file
	 * @return stream of data exist in the file
	 */
	@Override
	public Stream<String> fileTypeReader(Path filePath) {
		Stream<String> dataStream = null;
		try {
			dataStream = Files.lines(filePath);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return dataStream;
	}
}